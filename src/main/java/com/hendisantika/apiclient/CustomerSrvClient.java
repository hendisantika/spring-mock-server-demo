package com.hendisantika.apiclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hendisantika.model.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-mock-server-demo
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/27/22
 * Time: 07:54
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@Component
public class CustomerSrvClient {

    private final String customerSrvUrl;

    private final WebClient webClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    public CustomerSrvClient(@Value("${app.customerSrvUrl}") String customerSrvUrl) {
        this.customerSrvUrl = customerSrvUrl;
        this.webClient = WebClient.builder().baseUrl(customerSrvUrl)
                .build();
    }

    public Optional<Customer> getCustomer(Long id) {

        WebClient webClient = WebClient.create(customerSrvUrl);
        Mono<Customer> customer = webClient.get()
                .uri("/customers/" + id)
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals, clientResponse -> Mono.empty())
                .bodyToMono(Customer.class);

        return customer.blockOptional();
    }

    public List<Customer> getCustomers() {

        WebClient webClient = WebClient.create(customerSrvUrl);
        Flux<Customer> customers = webClient.get()
                .uri("/customers")
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals, clientResponse -> Mono.empty())
                .bodyToFlux(Customer.class);

        return customers.collectList().block();
    }

}
