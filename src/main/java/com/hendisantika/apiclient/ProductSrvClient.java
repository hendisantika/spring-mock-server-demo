package com.hendisantika.apiclient;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hendisantika.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-mock-server-demo
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/27/22
 * Time: 07:56
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ProductSrvClient {

    private final String productSrvUrl;

    private final WebClient webClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    public ProductSrvClient(@Value("${app.productSrvUrl}") String productSrvUrl) {
        this.productSrvUrl = productSrvUrl;
        this.webClient = WebClient.builder().baseUrl(productSrvUrl)
                .build();
    }

    public Optional<Product> getProduct(String id) {
        WebClient webClient = WebClient.create(productSrvUrl);
        Mono<Product> products = webClient.get()
                .uri("/products/" + id)
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals, clientResponse -> Mono.empty())
                .bodyToMono(Product.class);

        return products.blockOptional();
    }

    public List<Product> getProducts() {

        WebClient webClient = WebClient.create(productSrvUrl);
        Flux<Product> products = webClient.get()
                .uri("/products")
                .retrieve()
                .onStatus(HttpStatus.NOT_FOUND::equals, clientResponse -> Mono.empty())
                .bodyToFlux(Product.class);

        return products.collectList().block();
    }
}
