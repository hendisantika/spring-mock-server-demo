package com.hendisantika.apiclient;

import com.hendisantika.dto.QuotationEngineReq;
import com.hendisantika.model.Quotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-mock-server-demo
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/27/22
 * Time: 07:57
 * To change this template use File | Settings | File Templates.
 */
@Component
public class QuotationEngineClient {

    private final String quotationEngineSrvUrl;

    private final WebClient webClient;

    @Autowired
    public QuotationEngineClient(@Value("${app.quotationEngineSrvUrl}") String quotationEngineSrvUrl) {
        this.quotationEngineSrvUrl = quotationEngineSrvUrl;
        this.webClient = WebClient.builder().baseUrl(quotationEngineSrvUrl)
                .build();
    }

    public Quotation generateQuotation(QuotationEngineReq req) {
        WebClient webClient = WebClient.create(quotationEngineSrvUrl);
        Mono<Quotation> quotation = webClient.post()
                .uri("/quotation/generate")
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(req), QuotationEngineReq.class)
                .retrieve()
                .bodyToMono(Quotation.class);

        return quotation.block();
    }
}
