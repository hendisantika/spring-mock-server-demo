package com.hendisantika.controller;

import com.hendisantika.dto.QuotationReq;
import com.hendisantika.exception.QuotationCriteriaNotFulfilledException;
import com.hendisantika.exception.RecordNotFoundException;
import com.hendisantika.model.Quotation;
import com.hendisantika.service.QuotationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-mock-server-demo
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/27/22
 * Time: 08:01
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@RestController
@RequestMapping("/quotations")
@RequiredArgsConstructor
public class QuotationRestController {

    private final QuotationService quotationService;

    @GetMapping(value = {"/{id}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public Quotation getQuotation(@PathVariable String id) {

        Optional<Quotation> quotation = quotationService.fetchQuotation(id);
        if (quotation.isPresent()) {
            return quotation.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Quotation record not found");
        }
    }

    @PostMapping(value = {"/generate"}, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public Quotation generateQuotation(@Valid @RequestBody QuotationReq req) throws IOException, RecordNotFoundException, QuotationCriteriaNotFulfilledException {
        return quotationService.generateQuotation(req);
    }
}
