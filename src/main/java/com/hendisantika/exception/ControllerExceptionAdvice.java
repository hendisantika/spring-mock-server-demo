package com.hendisantika.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-mock-server-demo
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/27/22
 * Time: 07:51
 * To change this template use File | Settings | File Templates.
 */
@ControllerAdvice
public class ControllerExceptionAdvice {

    @ExceptionHandler({QuotationCriteriaNotFulfilledException.class})
    public final ResponseEntity<String> handleQuotationException(Exception ex) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }

    @ExceptionHandler({RecordNotFoundException.class})
    public final ResponseEntity<String> handleRecordNotFoundException(Exception ex) {
        return ResponseEntity.badRequest().body(ex.getMessage());
    }
}
