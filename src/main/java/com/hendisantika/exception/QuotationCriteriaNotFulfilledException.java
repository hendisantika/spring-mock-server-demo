package com.hendisantika.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-mock-server-demo
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/27/22
 * Time: 07:51
 * To change this template use File | Settings | File Templates.
 */
public class QuotationCriteriaNotFulfilledException extends Exception {

    public QuotationCriteriaNotFulfilledException() {
        super();
    }

    public QuotationCriteriaNotFulfilledException(String msg) {
        super(msg);
    }
}
