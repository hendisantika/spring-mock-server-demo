package com.hendisantika.repository;

import com.hendisantika.model.Quotation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-mock-server-demo
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/26/22
 * Time: 22:05
 * To change this template use File | Settings | File Templates.
 */
@Repository
public interface QuotationRepository extends CrudRepository<Quotation, String> {
}
