package com.hendisantika.service;

import com.hendisantika.apiclient.CustomerSrvClient;
import com.hendisantika.apiclient.ProductSrvClient;
import com.hendisantika.apiclient.QuotationEngineClient;
import com.hendisantika.dto.QuotationEngineReq;
import com.hendisantika.dto.QuotationReq;
import com.hendisantika.exception.QuotationCriteriaNotFulfilledException;
import com.hendisantika.exception.RecordNotFoundException;
import com.hendisantika.model.Customer;
import com.hendisantika.model.Product;
import com.hendisantika.model.Quotation;
import com.hendisantika.repository.QuotationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-mock-server-demo
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/26/22
 * Time: 22:06
 * To change this template use File | Settings | File Templates.
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class QuotationService {

    public static final int CUSTOMER_ELIGIBLE_AGE = 18;

    private final QuotationRepository quotationRepo;

    private final QuotationEngineClient quotationEngineClient;

    private final CustomerSrvClient customerSrvClient;

    private final ProductSrvClient productSrvClient;

    public Quotation generateQuotation(QuotationReq request) throws IOException, RecordNotFoundException, QuotationCriteriaNotFulfilledException {

        // get customer info
        Optional<Customer> customerOptional = customerSrvClient.getCustomer(request.getCustomerId());
        Customer customer = customerOptional.orElseThrow(() -> new RecordNotFoundException("Unknown customer"));

        // customer's age should be 18 or above
        LocalDateTime now = LocalDateTime.now();
        Period period = Period.between(customer.getDob(), now.toLocalDate());
        if (period.getYears() < CUSTOMER_ELIGIBLE_AGE) {
            throw new QuotationCriteriaNotFulfilledException("customer's age < 18");
        }

        // get product spec
        Optional<Product> productOptional = productSrvClient.getProduct(request.getProductCode());
        Product product = productOptional.orElseThrow(() -> new RecordNotFoundException("Unknown product"));

        log.info("Retrieved product: " + product.toString());
        log.info("Request Post Cocd: " + request);

        // the request post code should be within the product's service scope
        if (!Stream.of(product.getPostCodeInService()).anyMatch(s -> s.equalsIgnoreCase(request.getPostCode()))) {
            throw new QuotationCriteriaNotFulfilledException(String.format("Request post code %s is not within the scope of service", request.getPostCode()));
        }

        QuotationEngineReq quotationEngineReq = new QuotationEngineReq(customer, product);
        Quotation quotation = quotationEngineClient.generateQuotation(quotationEngineReq);

        quotationRepo.save(quotation);

        return quotation;

    }

    public Optional<Quotation> fetchQuotation(String quotationCode) {
        return quotationRepo.findById(quotationCode);
    }
}
