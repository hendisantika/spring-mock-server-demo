package com.hendisantika.mockserver;

import org.mockserver.client.MockServerClient;
import org.mockserver.client.initialize.PluginExpectationInitializer;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-mock-server-demo
 * User: powercommerce
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 9/28/22
 * Time: 21:48
 * To change this template use File | Settings | File Templates.
 */
public class QuotationMockServerInitialization implements PluginExpectationInitializer {

    @Override
    public void initializeExpectations(MockServerClient mockServerClient) {
        MockServerExpectationInitializer initializer = new MockServerExpectationInitializer(mockServerClient);
        initializer.initializeForIntegrationTest();
    }
}
